#!/bin/sh

# Copyright 2013 Giulio Paci <giuliopaci@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the
#   distribution.
#
# * Neither the name of the Massachusetts Institute of Technology nor
#   the names of its contributors may be used to endorse or promote
#   products derived from this software without specific prior written
#   permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

VERSION=0.4

cmd=interpolate-ngram
help2man --version-string="$VERSION" --no-info --help-option=-h \
    -n "interpolates n\\-gram language models" \
    --source=MITLM "$CMDSPATH$cmd" \
    | head -n -5 \
    | awk '!p || !/[.]IP/; /^\\fB\\-/{p=1}; /^[^\\]/{p=0};' \
    | sed -e 's/^.HP$/.TP/g' > "$cmd".1
cat<<EOF >> "$cmd".1
.SH SEE ALSO
\fBestimate-ngram\fR(1), \fBevaluate-ngram\fR(1)
EOF

cmd=evaluate-ngram
help2man --version-string="$VERSION" --no-info --help-option=-h \
    -n "evaluates n\\-gram language model" \
    --source=MITLM "$CMDSPATH$cmd" \
    | head -n -5 \
    | awk '!p || !/[.]IP/; /^\\fB\\-/{p=1}; /^[^\\]/{p=0};' \
    | sed -e 's/^.HP$/.TP/g' > "$cmd".1
cat<<EOF >> "$cmd".1
.SH SEE ALSO
\fBestimate-ngram\fR(1), \fBinterpolate-ngram\fR(1)
EOF

cmd=estimate-ngram
help2man --version-string="$VERSION" --no-info --help-option=-h \
    -n "estimates n\\-gram language model" \
    --source=MITLM "$CMDSPATH$cmd" \
    | head -n -5 \
    | awk '!p || !/[.]IP/; /^\\fB\\-/{p=1}; /^[^\\]/{p=0};' \
    | sed -e 's/^.HP$/.TP/g' > "$cmd".1
cat<<EOF >> "$cmd".1
.SH SEE ALSO
\fBevaluate-ngram\fR(1), \fBinterpolate-ngram\fR(1)
EOF
